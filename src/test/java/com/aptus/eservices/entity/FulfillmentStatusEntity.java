package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@DynamoDBDocument
@JsonIgnoreProperties(ignoreUnknown = true)
public class FulfillmentStatusEntity {

    private String status;

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }
}
