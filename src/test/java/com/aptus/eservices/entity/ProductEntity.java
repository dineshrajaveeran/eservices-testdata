package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;

@AutoProperty
@DynamoDBTable(tableName = "Product")
public class ProductEntity extends BaseTableEntity {

    private String id;
    private String type;
    private String shortDescription;
    private String longDescription;
    private String fulfillmentProcessId;
    private String manufacturerId;

    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey(attributeName = "id")
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "type")
    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    @DynamoDBAttribute(attributeName = "shortDescription")
    public String getShortDescription() {

        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {

        this.shortDescription = shortDescription;
    }

    @DynamoDBAttribute(attributeName = "longDescription")
    public String getLongDescription() {

        return longDescription;
    }

    public void setLongDescription(String longDescription) {

        this.longDescription = longDescription;
    }

    @DynamoDBAttribute(attributeName = "fulfillmentProcessId")
    public String getFulfillmentProcessId() {

        return fulfillmentProcessId;
    }

    public void setFulfillmentProcessId(String fulfillmentProcessId) {

        this.fulfillmentProcessId = fulfillmentProcessId;
    }

    @DynamoDBAttribute(attributeName = "manufacturerId")
    public String getManufacturerId() {

        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {

        this.manufacturerId = manufacturerId;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
