package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.List;

@AutoProperty
@DynamoDBDocument
public class AllocationGroupEntity extends BaseEntity {

    private List<String> products;
    private int maxQuantity;
    private AllocationCycleEntity cycle;

    public List<String> getProducts() {

        return products;
    }

    public void setProducts(List<String> products) {

        this.products = products;
    }

    public int getMaxQuantity() {

        return maxQuantity;
    }

    public void setMaxQuantity(int maxQuantity) {

        this.maxQuantity = maxQuantity;
    }

    public AllocationCycleEntity getCycle() {

        return cycle;
    }

    public void setCycle(AllocationCycleEntity cycle) {

        this.cycle = cycle;
    }
}