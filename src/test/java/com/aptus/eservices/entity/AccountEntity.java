package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@DynamoDBTable(tableName = "Account")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountEntity extends BaseTableEntity {

    private String id;
    private String userId;
    private String email;
    private String emailLowerCase;
    private String providerId;
    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {

        this.userId = userId;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getEmailLowerCase() {

        return emailLowerCase;
    }

    public void setEmailLowerCase(String emailLowerCase) {

        this.emailLowerCase = emailLowerCase;
    }

    public String getProviderId() {

        return providerId;
    }

    public void setProviderId(String providerId) {

        this.providerId = providerId;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
