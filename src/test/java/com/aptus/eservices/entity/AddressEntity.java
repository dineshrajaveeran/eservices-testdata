package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDBDocument
public class AddressEntity extends BaseEntity {

    private String label;
    private String line1;
    private String line2;
    private String line3;
    private String city;
    private String state;
    private String postalCode;
    private String country;

    public String getLabel() {

        return label;
    }

    public void setLabel(String label) {

        this.label = label;
    }

    public String getLine1() {

        return line1;
    }

    public void setLine1(String line1) {

        this.line1 = line1;
    }

    public String getLine2() {

        return line2;
    }

    public void setLine2(String line2) {

        this.line2 = line2;
    }

    public String getLine3() {

        return line3;
    }

    public void setLine3(String line3) {

        this.line3 = line3;
    }

    public String getCity() {

        return city;
    }

    public void setCity(String city) {

        this.city = city;
    }

    public String getState() {

        return state;
    }

    public void setState(String state) {

        this.state = state;
    }

    public String getPostalCode() {

        return postalCode;
    }

    public void setPostalCode(String postalCode) {

        this.postalCode = postalCode;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }
}
