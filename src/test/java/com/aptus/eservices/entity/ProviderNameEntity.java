package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
@DynamoDBDocument
public class ProviderNameEntity extends BaseEntity {

    String title;
    String first;
    String middle;
    String last;
    String suffix;
    String alternateFirst;
    String alternateLast;
    String full;

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getFirst() {

        return first;
    }

    public void setFirst(String first) {

        this.first = first;
    }

    public String getMiddle() {

        return middle;
    }

    public void setMiddle(String middle) {

        this.middle = middle;
    }

    public String getLast() {

        return last;
    }

    public void setLast(String last) {

        this.last = last;
    }

    public String getSuffix() {

        return suffix;
    }

    public void setSuffix(String suffix) {

        this.suffix = suffix;
    }

    public String getAlternateFirst() {

        return alternateFirst;
    }

    public void setAlternateFirst(String alternateFirst) {

        this.alternateFirst = alternateFirst;
    }

    public String getAlternateLast() {

        return alternateLast;
    }

    public void setAlternateLast(String alternateLast) {

        this.alternateLast = alternateLast;
    }

    public String getFull() {

        return full;
    }

    public void setFull(String full) {

        this.full = full;
    }
}
