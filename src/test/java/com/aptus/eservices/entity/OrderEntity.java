package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

@DynamoDBTable(tableName = "Order")
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderEntity extends BaseTableEntity {

    private String id;
    private String accountId;
    private String providerId;
    private OrderProviderEntity provider;
    private List<ProductRequestEntity> requests;
    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey(attributeName = "id")
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "accountId")
    public String getAccountId() {

        return accountId;
    }

    public void setAccountId(String accountId) {

        this.accountId = accountId;
    }

    @DynamoDBAttribute(attributeName = "providerId")
    public String getProviderId() {

        return providerId;
    }

    public void setProviderId(String providerId) {

        this.providerId = providerId;
    }

    @DynamoDBAttribute(attributeName = "provider")
    public OrderProviderEntity getProvider() {

        return provider;
    }

    public void setProvider(OrderProviderEntity provider) {

        this.provider = provider;
    }

    @DynamoDBAttribute(attributeName = "requests")
    public List<ProductRequestEntity> getRequests() {

        return requests;
    }

    public void setRequests(List<ProductRequestEntity> requests) {

        this.requests = requests;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
