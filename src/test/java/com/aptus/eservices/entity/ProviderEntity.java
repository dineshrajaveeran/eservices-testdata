package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;

@AutoProperty
@DynamoDBTable(tableName = "Provider")
public class ProviderEntity extends BaseTableEntity {

    private String id;
    private String thirdPartyKey;
    private Date detailExpiryDate;
    private Date lastDetailReceivedDate;
    private ProviderNameEntity name;
    private AddressEntity primaryAddress;
    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getThirdPartyKey() {

        return thirdPartyKey;
    }

    public void setThirdPartyKey(String thirdPartyKey) {

        this.thirdPartyKey = thirdPartyKey;
    }

    public ProviderNameEntity getName() {

        return name;
    }

    public void setName(ProviderNameEntity name) {

        this.name = name;
    }

    public AddressEntity getPrimaryAddress() {

        return primaryAddress;
    }

    public void setPrimaryAddress(AddressEntity primaryAddress) {

        this.primaryAddress = primaryAddress;
    }

    public Date getLastDetailReceivedDate() {

        return lastDetailReceivedDate;
    }

    public void setLastDetailReceivedDate(Date lastDetailReceivedDate) {

        this.lastDetailReceivedDate = lastDetailReceivedDate;
    }

    public Date getDetailExpiryDate() {

        return this.detailExpiryDate;
    }

    public void setDetailExpiryDate(Date detailExpiryDate) {

        this.detailExpiryDate = detailExpiryDate;
    }

    @Override
    public Date getCreatedDate() {

        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return this.lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return this.modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
