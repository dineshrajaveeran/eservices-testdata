package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.List;

@AutoProperty
@DynamoDBDocument
public class AllocationTierEntity extends BaseEntity {

    private String name;
    private List<AllocationGroupEntity> allocationGroups;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public List<AllocationGroupEntity> getAllocationGroups() {

        return allocationGroups;
    }

    public void setAllocationGroups(List<AllocationGroupEntity> allocationGroups) {

        this.allocationGroups = allocationGroups;
    }
}