package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.List;

@AutoProperty
@DynamoDBDocument
public class StagedProductRequestDisplayGroupEntity extends BaseEntity {

    private DisplayGroupEntity displayGroup;
    private List<StagedProductRequestItemEntity> items;

    public DisplayGroupEntity getDisplayGroup() {

        return displayGroup;
    }

    public void setDisplayGroup(DisplayGroupEntity displayGroup) {

        this.displayGroup = displayGroup;
    }

    public List<StagedProductRequestItemEntity> getItems() {

        return items;
    }

    public void setItems(List<StagedProductRequestItemEntity> items) {

        this.items = items;
    }
}
