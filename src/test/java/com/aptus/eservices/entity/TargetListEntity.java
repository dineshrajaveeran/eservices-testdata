package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;
import java.util.List;

@AutoProperty
@DynamoDBTable(tableName = "AptusAllocation.TargetList")
public class TargetListEntity extends BaseTableEntity {

    private String name;
    private String allocationEngineId;
    private List<AllocationTierEntity> tiers;
    private AllocationTierEntity defaultTier;
    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getAllocationEngineId() {

        return allocationEngineId;
    }

    public void setAllocationEngineId(String allocationEngineId) {

        this.allocationEngineId = allocationEngineId;
    }

    public List<AllocationTierEntity> getTiers() {

        return tiers;
    }

    public void setTiers(List<AllocationTierEntity> tiers) {

        this.tiers = tiers;
    }

    public AllocationTierEntity getDefaultTier() {

        return defaultTier;
    }

    public void setDefaultTier(AllocationTierEntity defaultTier) {

        this.defaultTier = defaultTier;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}