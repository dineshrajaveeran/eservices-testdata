package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDBDocument
public class ExternalLinkEntity extends BaseEntity {

    private String type;
    private String text;
    private String url;

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }
}
