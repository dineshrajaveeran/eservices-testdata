package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
@DynamoDBDocument
public class TaxonomyEntity extends BaseEntity {

    private String code;
    private String name;

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }
}
