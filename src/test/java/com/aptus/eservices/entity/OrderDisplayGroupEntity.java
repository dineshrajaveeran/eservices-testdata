package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


@DynamoDBDocument
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDisplayGroupEntity {

    private String id;
    private String name;
    private String logoFile;
    private List<OrderManufacturerEntity> manufacturers;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getLogoFile() {

        return logoFile;
    }

    public void setLogoFile(String logoFile) {

        this.logoFile = logoFile;
    }

    public List<OrderManufacturerEntity> getManufacturers() {

        return manufacturers;
    }

    public void setManufacturers(List<OrderManufacturerEntity> manufacturers) {

        this.manufacturers = manufacturers;
    }
}
