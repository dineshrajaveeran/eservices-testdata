package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@DynamoDBDocument
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductRequestDisplayGroupEntity extends BaseEntity {

    private OrderDisplayGroupEntity displayGroup;
    private List<ProductRequestItemEntity> items;

    public OrderDisplayGroupEntity getDisplayGroup() {

        return displayGroup;
    }

    public void setDisplayGroup(OrderDisplayGroupEntity displayGroup) {

        this.displayGroup = displayGroup;
    }

    public List<ProductRequestItemEntity> getItems() {

        return items;
    }

    public void setItems(List<ProductRequestItemEntity> items) {

        this.items = items;
    }
}
