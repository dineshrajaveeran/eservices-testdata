package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
@DynamoDBDocument
public class AllocationCycleEntity extends BaseEntity {

    private String type;
    private Integer dayOfMonth;
    private Integer numberOfDays;

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public Integer getDayOfMonth() {

        return dayOfMonth;
    }

    public void setDayOfMonth(Integer dayOfMonth) {

        this.dayOfMonth = dayOfMonth;
    }

    public Integer getNumberOfDays() {

        return numberOfDays;
    }

    public void setNumberOfDays(Integer numberOfDays) {

        this.numberOfDays = numberOfDays;
    }
}