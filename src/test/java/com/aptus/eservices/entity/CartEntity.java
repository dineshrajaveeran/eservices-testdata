package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;
import java.util.List;

@AutoProperty
@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDBTable(tableName = "Cart")
public class CartEntity extends BaseTableEntity {

    private String id;
    private AddressEntity selectedAddress;
    private List<CartDisplayGroupEntity> displayGroups;
    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;
    private String providerId;

    @DynamoDBHashKey
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getProviderId() {

        return providerId;
    }

    public void setProviderId(String providerId) {

        this.providerId = providerId;
    }

    public AddressEntity getSelectedAddress() {

        return selectedAddress;
    }

    public void setSelectedAddress(AddressEntity selectedAddress) {

        this.selectedAddress = selectedAddress;
    }

    public List<CartDisplayGroupEntity> getDisplayGroups() {

        return displayGroups;
    }

    public void setDisplayGroups(List<CartDisplayGroupEntity> displayGroups) {

        this.displayGroups = displayGroups;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}

@DynamoDBDocument
@JsonIgnoreProperties(ignoreUnknown = true)
class CartDisplayGroupEntity{

    private String id;
    private List<CartProductEntity> products;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public List<CartProductEntity> getProducts() {

        return products;
    }

    public void setProducts(List<CartProductEntity> products) {

        this.products = products;
    }
}

@DynamoDBDocument
@JsonIgnoreProperties(ignoreUnknown = true)
class CartProductEntity{
    private String id;
    private int quantity;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public int getQuantity() {

        return quantity;
    }

    public void setQuantity(int quantity) {

        this.quantity = quantity;
    }
}
