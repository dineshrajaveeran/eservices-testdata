package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
@DynamoDBDocument
public class StagedProductRequestItemEntity extends BaseEntity {

    private ProductEntity product;
    private Integer quantity;

    public ProductEntity getProduct() {

        return product;
    }

    public void setProduct(ProductEntity product) {

        this.product = product;
    }

    public Integer getQuantity() {

        return quantity;
    }

    public void setQuantity(Integer quantity) {

        this.quantity = quantity;
    }
}
