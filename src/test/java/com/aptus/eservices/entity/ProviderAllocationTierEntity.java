package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;

@AutoProperty
@DynamoDBTable(tableName = "AptusAllocation.ProviderAllocationTier")
public class ProviderAllocationTierEntity extends BaseTableEntity {

    private String id;
    private String providerId;
    private String targetListName;
    private String allocationTierName;
    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getProviderId() {

        return providerId;
    }

    public void setProviderId(String providerId) {

        this.providerId = providerId;
    }

    public String getTargetListName() {

        return targetListName;
    }

    public void setTargetListName(String targetListName) {

        this.targetListName = targetListName;
    }

    public String getAllocationTierName() {

        return allocationTierName;
    }

    public void setAllocationTierName(String allocationTierName) {

        this.allocationTierName = allocationTierName;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}