package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.List;

@AutoProperty
@DynamoDBDocument
public class StagedProductRequestEntity extends BaseEntity {

    private AddressEntity address;
    private StateLicenseEntity stateLicense;
    private String deaNumber;
    private List<StagedProductRequestDisplayGroupEntity> displayGroups;
    private String fulfillmentProcessId;

    public AddressEntity getAddress() {

        return address;
    }

    public void setAddress(AddressEntity address) {

        this.address = address;
    }

    public StateLicenseEntity getStateLicense() {

        return stateLicense;
    }

    public void setStateLicense(StateLicenseEntity stateLicense) {

        this.stateLicense = stateLicense;
    }

    public String getDeaNumber() {

        return deaNumber;
    }

    public void setDeaNumber(String deaNumber) {

        this.deaNumber = deaNumber;
    }

    public List<StagedProductRequestDisplayGroupEntity> getDisplayGroups() {

        return displayGroups;
    }

    public void setDisplayGroups(List<StagedProductRequestDisplayGroupEntity> displayGroups) {

        this.displayGroups = displayGroups;
    }

    public String getFulfillmentProcessId() {

        return fulfillmentProcessId;
    }

    public void setFulfillmentProcessId(String fulfillmentProcessId) {

        this.fulfillmentProcessId = fulfillmentProcessId;
    }
}
