package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;

@AutoProperty
@DynamoDBDocument
public class ProductRequestItemEntity extends BaseEntity {

    private String productRequestId;
    private Date requestDate;
    private String productId;
    private Integer quantity;

    public String getProductRequestId() {

        return productRequestId;
    }

    public void setProductRequestId(String productRequestId) {

        this.productRequestId = productRequestId;
    }

    public Date getRequestDate() {

        return requestDate;
    }

    public void setRequestDate(Date requestDate) {

        this.requestDate = requestDate;
    }

    public String getProductId() {

        return productId;
    }

    public void setProductId(String productId) {

        this.productId = productId;
    }

    public Integer getQuantity() {

        return quantity;
    }

    public void setQuantity(Integer quantity) {

        this.quantity = quantity;
    }
}