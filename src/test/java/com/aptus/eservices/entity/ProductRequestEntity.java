package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

@DynamoDBTable(tableName = "ProductRequest")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductRequestEntity extends BaseTableEntity {

    private String id;
    private String orderId;
    private String requestNumber;
    private Date requestDate;
    private AddressEntity address;
    private StateLicenseEntity stateLicense;
    private String deaNumber;
    private List<ProductRequestDisplayGroupEntity> displayGroups;
    private String fulfillmentProcessId;

    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey(attributeName = "id")
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getOrderId() {

        return orderId;
    }

    public void setOrderId(String orderId) {

        this.orderId = orderId;
    }

    public String getFulfillmentProcessId() {

        return fulfillmentProcessId;
    }

    public void setFulfillmentProcessId(String fulfillmentProcessId) {

        this.fulfillmentProcessId = fulfillmentProcessId;
    }

    public String getRequestNumber() {

        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {

        this.requestNumber = requestNumber;
    }

    public Date getRequestDate() {

        return requestDate;
    }

    public void setRequestDate(Date requestDate) {

        this.requestDate = requestDate;
    }

    public AddressEntity getAddressEntity() {

        return address;
    }

    public void setAddressEntity(AddressEntity address) {

        this.address = address;
    }

    public StateLicenseEntity getStateLicense() {

        return stateLicense;
    }

    public void setStateLicense(StateLicenseEntity stateLicense) {

        this.stateLicense = stateLicense;
    }

    public String getDeaNumber() {

        return deaNumber;
    }

    public void setDeaNumber(String deaNumber) {

        this.deaNumber = deaNumber;
    }

    public List<ProductRequestDisplayGroupEntity> getDisplayGroups() {

        return displayGroups;
    }

    public void setDisplayGroups(List<ProductRequestDisplayGroupEntity> displayGroups) {

        this.displayGroups = displayGroups;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
