package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.aptus.eservices.test.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@DynamoDBDocument
@JsonIgnoreProperties(ignoreUnknown = true)
public class StateLicenseEntity extends BaseEntity {

    private String stateOfLicense;
    private String licenseNumber;

    public String getStateOfLicense() {

        return stateOfLicense;
    }

    public void setStateOfLicense(String stateOfLicense) {

        this.stateOfLicense = stateOfLicense;
    }

    public String getLicenseNumber() {

        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {

        this.licenseNumber = licenseNumber;
    }
}
