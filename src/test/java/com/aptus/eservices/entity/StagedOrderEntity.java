package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;
import java.util.List;

@AutoProperty
@DynamoDBTable(tableName = "StagedOrder")
public class StagedOrderEntity extends BaseTableEntity {

    private String id;
    private String providerId;
    private String partitionKey;
    private ProviderEntity provider;
    private List<StagedProductRequestEntity> requests;

    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey(attributeName = "id")
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "providerId")
    public String getProviderId() {

        return providerId;
    }

    public void setProviderId(String providerId) {

        this.providerId = providerId;
    }

    @DynamoDBAttribute(attributeName = "partitionKey")
    public String getPartitionKey() {

        return partitionKey;
    }

    public void setPartitionKey(String partitionKey) {

        this.partitionKey = partitionKey;
    }

    @DynamoDBAttribute(attributeName = "provider")
    public ProviderEntity getProvider() {

        return provider;
    }

    public void setProvider(ProviderEntity provider) {

        this.provider = provider;
    }

    @DynamoDBAttribute(attributeName = "requests")
    public List<StagedProductRequestEntity> getRequests() {

        return requests;
    }

    public void setRequests(List<StagedProductRequestEntity> requests) {

        this.requests = requests;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
