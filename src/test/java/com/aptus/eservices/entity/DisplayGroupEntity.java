package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;
import java.util.List;
import java.util.Set;

@AutoProperty
@DynamoDBTable(tableName = "DisplayGroup")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisplayGroupEntity extends BaseTableEntity {

    private String id;
    private String name;
    private String description;
    private String headerContent;
    private String summaryHeaderContent;
    private String logoFile;
    private Set<String> products;
    private Set<String> manufacturers;
    private List<ExternalLinkEntity> externalLinks;

    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey(attributeName = "id")
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getHeaderContent() {

        return headerContent;
    }

    public void setHeaderContent(String headerContent) {

        this.headerContent = headerContent;
    }

    public String getSummaryHeaderContent() {

        return summaryHeaderContent;
    }

    public void setSummaryHeaderContent(String summaryHeaderContent) {

        this.summaryHeaderContent = summaryHeaderContent;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getLogoFile() {

        return logoFile;
    }

    public void setLogoFile(String logoFile) {

        this.logoFile = logoFile;
    }

    public Set<String> getProducts() {

        return products;
    }

    public void setProducts(Set<String> products) {

        this.products = products;
    }

    public Set<String> getManufacturers() {

        return manufacturers;
    }

    public void setManufacturers(Set<String> manufacturers) {

        this.manufacturers = manufacturers;
    }

    public List<ExternalLinkEntity> getExternalLinks() {

        return externalLinks;
    }

    public void setExternalLinks(List<ExternalLinkEntity> externalLinks) {

        this.externalLinks = externalLinks;
    }

    @Override
    public Date getCreatedDate() {

        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
