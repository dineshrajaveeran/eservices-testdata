package com.aptus.eservices.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.aptus.eservices.test.common.BaseTableEntity;
import org.pojomatic.annotations.AutoProperty;

import java.util.Date;
import java.util.List;

@AutoProperty
@DynamoDBTable(tableName = "ProviderNpiRegistration")
public class ProviderNpiRegistrationEntity extends BaseTableEntity {

    private String id;
    private String providerId;
    private String npiNumber;
    private String primaryStateOfLicense;
    private String primaryStateLicenseNumber;
    private String primaryFormattedStateLicenseNumber;
    private String practiceAddressPhone;
    private String practiceAddressFax;
    private String mailingAddressPhone;
    private String mailingAddressFax;
    private String gender;
    private Date assignmentDate;
    private String degreeCredential;
    private Date lastUpdated;
    private String status;
    private String cmsValidatedStateOfLicense;
    private String cmsValidatedStateLicenseNumber;
    private boolean isOnPecosQrList;
    private ProviderNameEntity providerName;
    private ProviderNameEntity cmsValidatedProviderName;
    private TaxonomyEntity primaryTaxonomy;
    private List<TaxonomyEntity> secondaryTaxonomies;
    private AddressEntity practiceAddress;
    private AddressEntity mailingAddress;
    private Date createdDate;
    private Date lastModifiedDate;
    private Integer modelVersion;

    @DynamoDBHashKey
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getProviderId() {

        return providerId;
    }

    public void setProviderId(String providerId) {

        this.providerId = providerId;
    }

    public String getNpiNumber() {

        return npiNumber;
    }

    public void setNpiNumber(String npiNumber) {

        this.npiNumber = npiNumber;
    }

    public String getPrimaryStateOfLicense() {

        return primaryStateOfLicense;
    }

    public void setPrimaryStateOfLicense(String primaryStateOfLicense) {

        this.primaryStateOfLicense = primaryStateOfLicense;
    }

    public String getPrimaryStateLicenseNumber() {

        return primaryStateLicenseNumber;
    }

    public void setPrimaryStateLicenseNumber(String primaryStateLicenseNumber) {

        this.primaryStateLicenseNumber = primaryStateLicenseNumber;
    }

    public String getPrimaryFormattedStateLicenseNumber() {

        return primaryFormattedStateLicenseNumber;
    }

    public void setPrimaryFormattedStateLicenseNumber(String primaryFormattedStateLicenseNumber) {

        this.primaryFormattedStateLicenseNumber = primaryFormattedStateLicenseNumber;
    }

    public String getPracticeAddressPhone() {

        return practiceAddressPhone;
    }

    public void setPracticeAddressPhone(String practiceAddressPhone) {

        this.practiceAddressPhone = practiceAddressPhone;
    }

    public String getPracticeAddressFax() {

        return practiceAddressFax;
    }

    public void setPracticeAddressFax(String practiceAddressFax) {

        this.practiceAddressFax = practiceAddressFax;
    }

    public String getMailingAddressPhone() {

        return mailingAddressPhone;
    }

    public void setMailingAddressPhone(String mailingAddressPhone) {

        this.mailingAddressPhone = mailingAddressPhone;
    }

    public String getMailingAddressFax() {

        return mailingAddressFax;
    }

    public void setMailingAddressFax(String mailingAddressFax) {

        this.mailingAddressFax = mailingAddressFax;
    }

    public String getGender() {

        return gender;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    public Date getAssignmentDate() {

        return assignmentDate;
    }

    public void setAssignmentDate(Date assignmentDate) {

        this.assignmentDate = assignmentDate;
    }

    public String getDegreeCredential() {

        return degreeCredential;
    }

    public void setDegreeCredential(String degreeCredential) {

        this.degreeCredential = degreeCredential;
    }

    public Date getLastUpdated() {

        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {

        this.lastUpdated = lastUpdated;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getCmsValidatedStateOfLicense() {

        return cmsValidatedStateOfLicense;
    }

    public void setCmsValidatedStateOfLicense(String cmsValidatedStateOfLicense) {

        this.cmsValidatedStateOfLicense = cmsValidatedStateOfLicense;
    }

    public String getCmsValidatedStateLicenseNumber() {

        return cmsValidatedStateLicenseNumber;
    }

    public void setCmsValidatedStateLicenseNumber(String cmsValidatedStateLicenseNumber) {

        this.cmsValidatedStateLicenseNumber = cmsValidatedStateLicenseNumber;
    }

    public boolean isOnPecosQrList() {

        return isOnPecosQrList;
    }

    public void setOnPecosQrList(boolean onPecosQrList) {

        isOnPecosQrList = onPecosQrList;
    }

    public ProviderNameEntity getProviderName() {

        return providerName;
    }

    public void setProviderName(ProviderNameEntity providerName) {

        this.providerName = providerName;
    }

    public ProviderNameEntity getCmsValidatedProviderName() {

        return cmsValidatedProviderName;
    }

    public void setCmsValidatedProviderName(ProviderNameEntity cmsValidatedProviderName) {

        this.cmsValidatedProviderName = cmsValidatedProviderName;
    }

    public TaxonomyEntity getPrimaryTaxonomy() {

        return primaryTaxonomy;
    }

    public void setPrimaryTaxonomy(TaxonomyEntity primaryTaxonomy) {

        this.primaryTaxonomy = primaryTaxonomy;
    }

    public List<TaxonomyEntity> getSecondaryTaxonomies() {

        return secondaryTaxonomies;
    }

    public void setSecondaryTaxonomies(List<TaxonomyEntity> secondaryTaxonomies) {

        this.secondaryTaxonomies = secondaryTaxonomies;
    }

    public AddressEntity getPracticeAddress() {

        return practiceAddress;
    }

    public void setPracticeAddress(AddressEntity practiceAddress) {

        this.practiceAddress = practiceAddress;
    }

    public AddressEntity getMailingAddress() {

        return mailingAddress;
    }

    public void setMailingAddress(AddressEntity mailingAddress) {

        this.mailingAddress = mailingAddress;
    }

    @Override
    public Date getCreatedDate() {

        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {

        this.createdDate = createdDate;
    }

    @Override
    public Date getLastModifiedDate() {

        return this.lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public Integer getModelVersion() {

        return this.modelVersion;
    }

    @Override
    public void setModelVersion(Integer modelVersion) {

        this.modelVersion = modelVersion;
    }
}
