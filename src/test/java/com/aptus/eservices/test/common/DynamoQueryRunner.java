package com.aptus.eservices.test.common;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

import java.util.List;

public class DynamoQueryRunner implements IDynamoQueryRunner {

    private DynamoDBClient dbClient = new DynamoDBClient();

    @Override
    public <T extends BaseTableEntity> List<T> scan(Class<T> entityClass, DynamoDBScanExpression scanExpression) {

        DynamoDBMapper mapper = dbClient.getDBMapper();
        DynamoDBMapperConfig mapperConfig = dbClient.getDBMapperConfig();
        return mapper.scan(entityClass, scanExpression, mapperConfig);
    }

    @Override
    public <T extends BaseTableEntity> T load(Class<T> entityClass, String id) {

        DynamoDBMapper mapper = dbClient.getDBMapper();
        DynamoDBMapperConfig mapperConfig = dbClient.getDBMapperConfig();
        return mapper.load(entityClass, id, mapperConfig);
    }

    @Override
    public <T extends BaseTableEntity> void save(BaseTableEntity obj) {

        DynamoDBMapper mapper = dbClient.getDBMapper();
        DynamoDBMapperConfig mapperConfig = dbClient.getDBMapperConfig();
        mapper.save(obj, mapperConfig);
    }

    @Override
    public void delete(BaseTableEntity obj) {

        DynamoDBMapper mapper = dbClient.getDBMapper();
        DynamoDBMapperConfig mapperConfig = dbClient.getDBMapperConfig();
        mapper.delete(obj, mapperConfig);
    }

    @Override
    public <T extends BaseTableEntity> List<T> query(Class<T> entityClass, DynamoDBQueryExpression queryExpression) {

        DynamoDBMapper mapper = dbClient.getDBMapper();
        DynamoDBMapperConfig mapperConfig = dbClient.getDBMapperConfig();
        return (List<T>) mapper.query(entityClass, queryExpression, mapperConfig);
    }
}
