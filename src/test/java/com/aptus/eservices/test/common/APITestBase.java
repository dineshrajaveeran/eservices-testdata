package com.aptus.eservices.test.common;

import com.aptus.eservices.test.utils.XMLInputLibrary;

import java.util.HashMap;
import java.util.Map;

public abstract class APITestBase {

    private final static String KEYCLOAK_REALM = "Realm";
    private final static String REALM_KEY = "RealmKey";
    private final static String KEYCLOAK_URL = "KeyCloakURL";
    private final static String ADMIN_USER_NAME = "AdminUserName";
    private final static String ADMIN_PASSWORD = "AdminPassword";
    private final static String SECRET = "Secret";
    private final static String CLIENT_ID = "ClientID";
    private static Map<String, String> paths;

    public APITestBase() {

        loadPaths(LoggerAndReader.getInstance().getXmlIn());
    }

    public static String keyCloakRealm() {return paths.get(KEYCLOAK_REALM);}

    public static String keyCloakUrl() {return paths.get(KEYCLOAK_URL);}

    public static String keyCloakSecret() {return paths.get(SECRET);}

    public static String adminUserName() {return paths.get(ADMIN_USER_NAME);}

    public static String adminPassword() {return paths.get(ADMIN_PASSWORD);}

    public static String clientID() {return paths.get(CLIENT_ID);}

    private void loadPaths(XMLInputLibrary xml) {

        paths = new HashMap<String, String>();
        paths.put(KEYCLOAK_REALM,
                  LoggerAndReader.getInstance().getMeThisProperty("keycloak.realm"));
        paths.put(REALM_KEY,
                  LoggerAndReader.getInstance().getMeThisProperty("keycloak.realmKey"));
        paths.put(KEYCLOAK_URL,
                  LoggerAndReader.getInstance().getMeThisProperty("keycloak.auth-server-url"));
        paths.put(ADMIN_USER_NAME,
                  LoggerAndReader.getInstance().getMeThisProperty("admin.username"));
        paths.put(ADMIN_PASSWORD,
                  LoggerAndReader.getInstance().getMeThisProperty("admin.password"));
        paths.put(SECRET,
                  LoggerAndReader.getInstance().getMeThisProperty("keycloak.credentials.secret"));
        paths.put(CLIENT_ID,
                  LoggerAndReader.getInstance().getMeThisProperty("keycloak.resource"));
    }
}
