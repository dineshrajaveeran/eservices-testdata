package com.aptus.eservices.test.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.pojomatic.Pojomatic;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseEntity {

    @Override
    public boolean equals(Object o) {

        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {

        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {

        return Pojomatic.toString(this);
    }
}
