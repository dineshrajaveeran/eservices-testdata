package com.aptus.eservices.test.common;

import com.aptus.eservices.test.utils.EServicesUtils;
import com.aptus.eservices.test.utils.XMLInputLibrary;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

public class LoggerAndReader {

    private static LoggerAndReader loggerAndReader;
    private static Properties props = null;
    private static String reportFileName;
    public Map<String, Object> responseAdditionalAttrMap = new Hashtable<String, Object>();
    private XMLInputLibrary xmlIn = null;
    private boolean status;
    private String resourceFolder;
    private String log4jPropertiesFileLoc;
    private String oAuth2ClientId = null;

    private String oAuth2tokenRequestUrl = null;

    private String oAuth2SecretToken = null;

    private String oauth2granttype = null;

    private String oauth2UserName = null;

    private String oauth2password = null;

    private String dbTablePrefix = null;

    public static LoggerAndReader getInstance() {

        if (loggerAndReader == null) {
            loggerAndReader = new LoggerAndReader();
        }
        if (props == null) {
            try {
                props = new Properties();
                FileInputStream is = new FileInputStream(new File(CommonBase.APITestPropertyFile));
                props.load(is);
            } catch (Exception e) {
                StringBuffer message =
                        new StringBuffer(
                                "\n Failed to load Automation Property File:" + CommonBase.APITestPropertyFile);
                message.append("\n Caused By:" + e.getMessage());
                Assert.fail(message.toString());
            }
            try {
                if (!loggerAndReader.status) {
                    loggerAndReader.initializeAutomation(CommonBase.TestFrameWorkSettingXmlFile);
                }
            } catch (Exception e) {
                StringBuffer message =
                        new StringBuffer("\n Failed to load XML File:" + CommonBase.TestFrameWorkSettingXmlFile);
                message.append("\n Caused By:" + e.getMessage());
                Assert.fail(message.toString());
            }
        }
        return loggerAndReader;
    }

    public String getMeThisProperty(String prop) {

        return (props.getProperty(prop).trim());
    }

    private boolean initializeAutomation(String xmlInFile) {

        if (status == false) {
            if (xmlInFile == null || xmlInFile.equals("")) {
                return false;
            }
            if (xmlIn == null) {
                xmlIn = new XMLInputLibrary(xmlInFile);
            }
            try {
                setGlobalVariables(xmlIn);
                status = true;
            } catch (SecurityException e1) {
                StringBuffer message = new StringBuffer();
                message.append("Error on LoggerAndReader.initializeAutomation - [SecurityException] - ");
                message.append("\n Caused By:" + e1.getMessage());
                status = false;
                Assert.fail(message.toString());
            } catch (IllegalArgumentException e1) {
                StringBuffer message = new StringBuffer();
                message.append("Error on LoggerAndReader.initializeAutomation - [IllegalArgumentException] - ");
                message.append("\n Caused By:" + e1.getMessage());
                status = false;
                Assert.fail(message.toString());
            }
        }
        return status;
    }

    private void setGlobalVariables(XMLInputLibrary xmlIn2) {

        resourceFolder = xmlIn.readInputParameter("resourcefolder");
        if (StringUtils.equalsIgnoreCase(xmlIn.readInputParameter("run_in_dev"), "yes")) {
            // KeyCloak-SampleCenter
            setoAuth2tokenRequestUrl(getMeThisProperty("dev_oauth_accessTokenUri"));
            setoAuth2ClientId(getMeThisProperty("dev_oauth_clientId"));
            setoAuth2SecretToken(getMeThisProperty("dev_oauth_clientSecret"));
            setOauth2granttype(getMeThisProperty("dev_oauth_grantType"));
            setOauth2UserName(getMeThisProperty("dev_oauth_username"));
            setOauth2password(getMeThisProperty("dev_oauth_password"));
            setDbTablePrefix(getMeThisProperty("db_table_prefix_dev"));
        } else if (StringUtils.equalsIgnoreCase(xmlIn.readInputParameter("run_in_int"), "yes")) {
            setDbTablePrefix(getMeThisProperty("db_table_prefix_int"));
        } else if (StringUtils.equalsIgnoreCase(xmlIn.readInputParameter("run_in_test"), "yes")) {
            setDbTablePrefix(getMeThisProperty("db_table_prefix_test"));
        } else if (StringUtils.equalsIgnoreCase(xmlIn.readInputParameter("run_in_preprod"), "yes")) {
            setDbTablePrefix(getMeThisProperty("db_table_prefix_preprod"));
        }
    }

    public String getDbTablePrefix() {

        return dbTablePrefix;
    }

    public void setDbTablePrefix(String dbTablePrefix) {

        this.dbTablePrefix = dbTablePrefix;
    }

    public String getReportFileName() {

        if (reportFileName != null) {
            return reportFileName;
        } else {
            Date now = new Date();
            reportFileName = "eServices_" + EServicesUtils.dateToDayAndTimeForFileName(now) + ".html";
            return reportFileName;
        }
    }

    public XMLInputLibrary getXmlIn() {

        return xmlIn;
    }

    public String getoAuth2ClientId() {

        return oAuth2ClientId;
    }

    public void setoAuth2ClientId(String oAuth2ClientId) {

        this.oAuth2ClientId = oAuth2ClientId;
    }

    public String getoAuth2tokenRequestUrl() {

        return oAuth2tokenRequestUrl;
    }

    public void setoAuth2tokenRequestUrl(String oAuth2tokenRequestUrl) {

        this.oAuth2tokenRequestUrl = oAuth2tokenRequestUrl;
    }

    public String getoAuth2SecretToken() {

        return oAuth2SecretToken;
    }

    public void setoAuth2SecretToken(String oAuth2SecretToken) {

        this.oAuth2SecretToken = oAuth2SecretToken;
    }

    public String getOauth2granttype() {

        return oauth2granttype;
    }

    public void setOauth2granttype(String oauth2granttype) {

        this.oauth2granttype = oauth2granttype;
    }

    public String getOauth2UserName() {

        return oauth2UserName;
    }

    public void setOauth2UserName(String oauth2UserName) {

        this.oauth2UserName = oauth2UserName;
    }

    public String getOauth2password() {

        return oauth2password;
    }

    public void setOauth2password(String oauth2password) {

        this.oauth2password = oauth2password;
    }
}
