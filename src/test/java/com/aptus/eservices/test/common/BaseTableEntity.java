package com.aptus.eservices.test.common;

import java.util.Date;

//Unfortunately DynamoDB Mapper does not support inheritance of attributes from base classes
//We'll have to do with abstract getters and setters for now
public abstract class BaseTableEntity extends BaseEntity {

    public abstract Date getCreatedDate();

    public abstract void setCreatedDate(Date createdDate);

    public abstract Date getLastModifiedDate();

    public abstract void setLastModifiedDate(Date lastModifiedDate);

    public abstract Integer getModelVersion();

    public abstract void setModelVersion(Integer modelVersion);
}
