package com.aptus.eservices.test.common;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

import java.util.List;

public interface IDynamoQueryRunner {

    public <T extends BaseTableEntity> List<T> scan(Class<T> clazz, DynamoDBScanExpression dynamoDBScanExpression);

    public <T extends BaseTableEntity> T load(Class<T> entityClass, String id);

    public <T extends BaseTableEntity> void save(BaseTableEntity obj);

    void delete(BaseTableEntity obj);

    <T extends BaseTableEntity> List<T> query(Class<T> clazz, DynamoDBQueryExpression queryExpression);
}
