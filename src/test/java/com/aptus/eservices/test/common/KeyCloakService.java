package com.aptus.eservices.test.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeyCloakService extends APITestBase{

    private static final Logger LOGGER = LogManager.getLogger();

    Keycloak keycloak;

    public KeyCloakService() {
        keycloak = KeycloakBuilder.builder()
                .serverUrl(APITestBase.keyCloakUrl())
                .realm(APITestBase.keyCloakRealm())
                .clientId(APITestBase.clientID())
                .username(APITestBase.adminUserName())
                .password(APITestBase.adminPassword())
                .clientSecret(APITestBase.keyCloakSecret())
                .build();
    }

    public String createNewUser(String firstName, String lastName, String email, String password, String accountId) {

        CredentialRepresentation credentials = new CredentialRepresentation();
        credentials.setType(CredentialRepresentation.PASSWORD);
        credentials.setValue(password);

        UserRepresentation user = new UserRepresentation();
        user.setUsername(email);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCredentials(Arrays.asList(credentials));
        user.setEnabled(true);
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("accountId", new String[] {accountId});
        user.setAttributes(attributes);

        javax.ws.rs.core.Response response = keycloak.realm(APITestBase.keyCloakRealm()).users().create(user);

        String locationHeader = response.getHeaderString("Location");
        String userId = locationHeader.replaceAll(".*/(.*)$", "$1");

        UserResource userResource = keycloak.realm(APITestBase.keyCloakRealm()).users().get(userId);
        userResource.resetPassword(credentials);

        return userId;
    }

    public void createUser(String firstName,
                           String lastName,
                           String email,
                           String password,
                           Map<String, Object> attributes) {

        CredentialRepresentation credentials = new CredentialRepresentation();
        credentials.setType(CredentialRepresentation.PASSWORD);
        credentials.setValue(password);
        credentials.setTemporary(false);

        UserRepresentation user = new UserRepresentation();
        user.setUsername(email);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCredentials(Collections.singletonList(credentials));
        user.setAttributes(attributes);
        user.setEnabled(true);
        user.setEmailVerified(true);

        Response response = keycloak.realm(APITestBase.keyCloakRealm()).users().create(user);

        if (!HttpStatus.valueOf(response.getStatus()).is2xxSuccessful()) {
            LOGGER.error("Error while attempting to create user in Keycloak");
            throw new RuntimeException("Error attempting to create user in Keycloak");
        }
    }

    public void updateUser(String userId) {

        UserResource resource = keycloak.realm(APITestBase.keyCloakRealm()).users().get(userId);
        UserRepresentation representation = resource.toRepresentation();
        representation.setEmailVerified(true);
        representation.setEnabled(true);

        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType("password");
        credentialRepresentation.setValue("abcd1234");

        resource.resetPassword(credentialRepresentation);

        resource.update(representation);
    }

    public void deleteUser(String userId) {

        keycloak.realm(APITestBase.keyCloakRealm()).users().get(userId).remove();
    }

    public void deleteUserByEmail(String email) {

        try {
            List<UserRepresentation> matchingUsers =
                    keycloak.realm(APITestBase.keyCloakRealm()).users().search(email, null, null, null, null, null);

            for (UserRepresentation user : matchingUsers) {
                try {
                    keycloak.realm(APITestBase.keyCloakRealm()).users().get(user.getId()).remove();
                } catch (Throwable ex) {
                    //Continue deleting other users even if this one fails to delete
                }
            }
        } catch (Throwable e) {
            //No need to throw exception on a setup method
        }
    }

    public UserRepresentation getUser(String email) {

        List<UserRepresentation> users = keycloak
                .realm(APITestBase.keyCloakRealm())
                .users()
                .search(email, null, null, null, null, null);

        if (users == null || users.size() == 0) {
            LOGGER.error("Searching for user by username did not return any results");
            throw new RuntimeException("Searching for user by username did not return any results");
        }

        if (users.size() > 1) {
            LOGGER.error("Searching for user by username returned more than one result");
            throw new RuntimeException("Searching for user by username returned more than one result");
        }

        return users.get(0);
    }

    public void affirmPasword(String id, String password) {

        CredentialRepresentation credentials = new CredentialRepresentation();
        credentials.setType(CredentialRepresentation.PASSWORD);
        credentials.setValue(password);
        credentials.setTemporary(false);

        keycloak.realm(APITestBase.keyCloakRealm()).users().get(id).resetPassword(credentials);
    }

    public List<UserRepresentation> getAllUsers() {

        return keycloak
                .realm(APITestBase.keyCloakRealm())
                .users()
                .search(null, null, null, null, null, null);
    }
}
