package com.aptus.eservices.test.common;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class DynamoDBClient {

    private static final Logger logger = LogManager.getLogger(DynamoDBClient.class);

    private AmazonDynamoDB dynamoDB;

    // Return a dynamoDB client instance
    public AmazonDynamoDB getDBClient() {

        if (dynamoDB != null) {
            return dynamoDB;
        }
        dynamoDB = new AmazonDynamoDBClient();
        setRegion();
        return dynamoDB;
    }

    private void setRegion() {

        String regionName = System.getenv("dynamo.region");
        if (regionName == null || regionName.length() == 0) {
            logger.warn("Missing environment variable 'dynamo.region'.  Defaulting to us-east-1.");
            regionName = Regions.US_EAST_1.getName();
        } else {
            logger.debug(String.format("Read value '%s' from dynamo.region environment variable.", regionName));
        }

        Regions regions;
        try {
            regions = Regions.fromName(regionName);
        } catch (IllegalArgumentException ex) {
            logger.warn(String.format("Unrecognized dynamo region '%s'. Defaulting to us-east-1.", regionName));
            regions = Regions.US_EAST_1;
        }

        Region region = Region.getRegion(regions);
        dynamoDB.setRegion(region);
    }

    // Return a dynamoDBMapper instance
    public DynamoDBMapper getDBMapper() {

        DynamoDBMapper mapper = new DynamoDBMapper(getDBClient());
        return mapper;
    }

    public DynamoDBMapperConfig getDBMapperConfig() {

        return new DynamoDBMapperConfig(DynamoDBMapperConfig.TableNameOverride.withTableNamePrefix(LoggerAndReader.getInstance()
                                                                                                           .getDbTablePrefix()));
    }
}
