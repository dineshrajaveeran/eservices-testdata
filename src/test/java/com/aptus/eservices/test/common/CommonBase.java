package com.aptus.eservices.test.common;

import com.aptus.eservices.test.exception.EServicesException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.apache.commons.lang3.RandomStringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Random;

/**
 * Utility Class for various operations with Integration Tests scope only
 *
 * @param <T>
 */
public class CommonBase<T> {

    public final static String TestFrameWorkSettingXmlFile = "src/test/resources/APIFrameworkParam.xml";

    public final static String APITestPropertyFile = "src/test/resources/test_environment_config.properties";

    /**
     * Generate a Random ASCII for the length specified
     *
     * @param length
     * @return
     */
    public static String generateRandomAscii(int length) {

        try {
            return RandomStringUtils.randomAlphanumeric(length);
        } catch (Exception e) {
            throw new EServicesException("Failed in creating a random ASCII", e);
        }
    }

    /**
     * Generate a Random String for the length specified
     *
     * @param length
     * @return
     */
    public static String generateRandomAlphabetic(int length) {

        try {
            return RandomStringUtils.randomAlphabetic(length);
        } catch (Exception e) {
            throw new EServicesException("Failed in creating a random string", e);
        }
    }

    /**
     * Generate a Random ASCII for length range specified
     *
     * @param minLength
     * @param maxLength
     * @return
     */
    public static String generateRandomAscii(int minLength, int maxLength) {

        int length = 0;
        Random r = new Random(maxLength);
        length = r.nextInt(maxLength);
        while (length < minLength) {
            length = r.nextInt(maxLength);
        }
        return generateRandomAscii(length);
    }

    /**
     * Generate a Random Integer
     *
     * @return
     */
    public static String generateRandomIntegers(int length) {

        try {
            Random r = new Random();
            String random = r.nextInt(99999) + "" + r.nextInt(99999) + r.nextInt(99999) + r.nextInt(99999)
                    + r.nextInt(99999) + r.nextInt(99999) + r.nextInt(99999) + r.nextInt(99999) + r.nextInt(99999)
                    + r.nextInt(99999) + r.nextInt(99999) + r.nextInt(99999) + r.nextInt(99999);
            return random.substring(0, length);
        } catch (Exception e) {
            throw new EServicesException("Failed in creating a random number", e);
        }
    }

    /**
     * Convert Object to a Readable String
     *
     * @param o
     * @return
     */
    public static String convertToRead(Object o) {

        ObjectMapper mapper = new ObjectMapper()
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
        String obj = "";
        try {
            obj = mapper.writeValueAsString(o);
        } catch (com.fasterxml.jackson.core.JsonGenerationException e) {
            throw new EServicesException(
                    "Exception occured while parsing Object to String :: " + e.getLocalizedMessage(),
                    e);
        } catch (com.fasterxml.jackson.databind.JsonMappingException e) {
            throw new EServicesException(
                    "Exception occured while parsing Object to String :: " + e.getLocalizedMessage(),
                    e);
        } catch (IOException e) {
            throw new EServicesException(
                    "Exception occured while parsing Object to String :: " + e.getLocalizedMessage(),
                    e);
        }
        return obj;
    }

    public Object convertToObject(String json, Class<T> clazz) {

        Object obj = null;
        try {
            ObjectMapper mapper = new ObjectMapper()
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                    .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
                    .registerModule(new ParameterNamesModule())
                    .registerModule(new Jdk8Module())
                    .registerModule(new JavaTimeModule());
            obj = mapper.readValue(json, clazz);
        } catch (JsonParseException e) {
            throw new EServicesException("Exception occured while parsing JSON to Object :: " + e.getLocalizedMessage(),
                                         e);
        } catch (JsonMappingException e) {
            throw new EServicesException("Exception occured while Mapping JSON to Object :: " + e.getLocalizedMessage(),
                                         e);
        } catch (IOException e) {
            throw new EServicesException(
                    "IO Exception occured while converting JSON to Object :: " + e.getLocalizedMessage(), e);
        }
        return obj;
    }
}
