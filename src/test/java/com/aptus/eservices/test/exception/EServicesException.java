package com.aptus.eservices.test.exception;

public class EServicesException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EServicesException(String message) {

        super(message);
    }

    public EServicesException(String message, Exception e) {

        super(message, e);
    }
}
