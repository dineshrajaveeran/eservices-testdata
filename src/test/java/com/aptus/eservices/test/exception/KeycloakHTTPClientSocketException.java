package com.aptus.eservices.test.exception;

import org.testng.SkipException;

public class KeycloakHTTPClientSocketException extends SkipException {

    private static final long serialVersionUID = 1L;

    public KeycloakHTTPClientSocketException(String skipMessage, Throwable e) {

        super(skipMessage, e);
    }

    public KeycloakHTTPClientSocketException(String skipMessage) {

        super(skipMessage);
    }
}
