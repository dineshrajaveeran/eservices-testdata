package com.aptus.eservices.test.utils;

import com.aptus.eservices.test.exception.EServicesException;
import org.testng.ITestResult;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class EServicesUtils {

    private final static Pattern LTRIM = Pattern.compile("^\\s+");

    private final static Pattern RTRIM = Pattern.compile("^\\s+$");

    /**
     * Mark time outs
     *
     * @param testResult
     */
    public static void markTimeoutFailureAsSkip(final ITestResult testResult) {

        if (null != testResult.getThrowable()
                &&
                testResult.getThrowable().getClass().equals(org.testng.internal.thread.ThreadTimeoutException.class)) {
            testResult.setStatus(ITestResult.SKIP);
        }
    }

    /**
     * Format stack trace
     *
     * @param t
     * @param b
     * @return String array
     */
    public static String[] stackTrace(Throwable t, boolean b) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        String[] arr = {sw.toString()};
        return arr;
    }

    /**
     * Convert object to String
     *
     * @param p
     * @return String
     */
    public static Object toString(Object o) {

        if (o != null) {
            return String.valueOf(o);
        } else {
            return null;
        }
    }

    /**
     * Convert phonenumber
     *
     * @param phone
     * @return
     */
    public static String convertPhoneNumber(String phone) {

        if (phone.contains("-")) {
            return phone;
        }

        return phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6, phone.length());
    }

    /**
     * Convert date to datetime
     *
     * @param startDate
     * @return date
     */
    public static String dateToDayAndTime(Date startDate) {

        DateFormat df = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        String sdt = df.format(startDate);
        return sdt;
    }

    /**
     * Convert date to requested string format
     *
     * @param startDate
     * @return date
     */
    public static String getDate(String format, Date date) {

        DateFormat df = new SimpleDateFormat(format);
        String sdt = df.format(date);
        return sdt;
    }

    /**
     * Convert Date to Datetime for fileNaming
     *
     * @param now
     * @return fileName with timeStamp
     */
    public static String dateToDayAndTimeForFileName(Date now) {

        DateFormat df = new SimpleDateFormat("yyMMddHHmmssZ");
        String sdt = df.format(now);
        return "_" + sdt;
    }

    /**
     * Handle escape chars
     *
     * @param o
     * @return String html
     */
    public static String escapeHtml(Object o) {

        if (o != null) {
            String toEscape = String.valueOf(o);
            String delim = "|";
            String regex = "(?<!\\\\)" + Pattern.quote(delim);
            StringBuilder builder = new StringBuilder();
            for (String s : toEscape.split(regex)) {
                builder.append(s);
            }
            return builder.toString();
        } else {
            return null;
        }
    }

    /**
     * @return Formatted String value of Date
     */
    public static String getDate() {

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Convert List to String Array
     *
     * @param list
     * @return
     */
    public static String[] listToArray(List<String> list) {

        int size = list.size();

        return list.toArray(new String[size]);
    }

    /**
     * Convert yyyy-MM-dd HH:mm:ss.SSS to MM/dd/yyyy
     *
     * @param dateString
     * @return
     */
    public static String formatDate(String dateString) {

        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);
            Date date = format.parse(dateString);
            format = new SimpleDateFormat("M/d/yyyy");
            String dateConverted = format.format(date);

            // Trim zero in Date and Month
            String part1 = dateConverted.substring(0, 6);
            String part2 = dateConverted.substring(6, dateConverted.length());

            return part1.concat(part2);
        } catch (ParseException e) {
            throw new EServicesException(
                    "Error converting Date type from yyyy-MM-dd HH:mm:ss.SSS to MM/dd/yyyy \n" +
                            e.getLocalizedMessage());
        }
    }

    /**
     * Left Trim String
     *
     * @param s
     * @return
     */
    public static String ltrim(String s) {

        return LTRIM.matcher(s).replaceAll("");
    }

    /**
     * Right trim a String
     *
     * @param s
     * @return
     */
    public static String rtrim(String s) {

        return RTRIM.matcher(s).replaceAll("");
    }
}
