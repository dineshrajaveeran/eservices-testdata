package com.aptus.eservices.test.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class XMLInputLibrary {

    private static Logger log = LogManager.getLogger(XMLInputLibrary.class);
    private static int num = 0;
    private XMLInputFactory inputFactory = null;
    private XMLStreamReader xmlReader = null;
    private String strAutoFrameworkXML = null;
    // Pull individual blocks and put them into Hashtables so that you read
    // input file just once
    private Map<String, String> inputParamsHT;
    private Map<String, String> inputSetupHT;
    // private Hashtable<String, String> childInputParamsHT;
    private Map<String, String> filenamesHT;
    private Map<String, String> groupsHT;
    private String executionMode = null;

    public XMLInputLibrary(String inputXML) {

        this.inputFactory = XMLInputFactory.newInstance();
        this.strAutoFrameworkXML = inputXML;
        inputParamsHT = new HashMap<String, String>();
        inputSetupHT = new HashMap<String, String>();
        filenamesHT = new HashMap<String, String>();
        groupsHT = new HashMap<String, String>();
        initHashTables();
    }

    private void initHashTables() {

        try {
            xmlReader = inputFactory.createXMLStreamReader(new FileReader(strAutoFrameworkXML));
            while (xmlReader.hasNext()) {
                Integer intEventType = xmlReader.next();
                if (intEventType.equals(XMLEvent.START_ELEMENT)) {
                    String str = xmlReader.getName().toString();
                    if (str.equals("InputParameter")) {
                        String strName = xmlReader.getAttributeValue(null, "Name");
                        String value = xmlReader.getAttributeValue(null, "Value");
                        inputParamsHT.put(strName, value);
                    } // Check for InputParameter
                    if (str.equals("Input")) {
                        String[] setupValues = {"OS", "Platform"};
                        for (String attribute : setupValues) {
                            String value = xmlReader.getAttributeValue(null, attribute);
                            inputSetupHT.put(attribute, value);
                        }
                    } // Check for InputParameter
                    if (str.contains("FileName")) {
                        String strName = xmlReader.getAttributeValue(null, "Name");
                        filenamesHT.put(str, strName);
                    } // Check for Files
                    if (str.equals("Group")) {
                        String strName = xmlReader.getAttributeValue(null, "Name");
                        // log.info(str+"--"+strName);
                        groupsHT.put(str + num, strName);
                        num++;
                    } // Check for Group
                    if (str.equals("Groups")) {
                        executionMode = xmlReader.getAttributeValue(null, "Run");
                    } // Check for Group
                } // Check For START_ELEMENT
            } // while loop for scanning XML
            xmlReader.close();
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
            // TODO
        } catch (XMLStreamException e) {
            log.error(e.getMessage());
            // TODO
        }
    }

    public String getFile() {

        return this.strAutoFrameworkXML;
    }

    public String readInputParameter(String strItem) {

        try {
            String strReturn;
            strReturn = inputParamsHT.get(strItem);
            return strReturn;
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public String readTestsetUp(String strItem) {

        try {
            String strReturn = null;
            strReturn = inputSetupHT.get(strItem);
            return strReturn;
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public String readResultOutputFile() {

        return readFileNames("ResultFileName");
    }

    public String readResultLogFile() {

        try {
            return readFileNames("LogFileName");
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public String readResultPerformanceFile() {

        return readFileNames("PerformanceFileName");
    }

    public String readFileNames(String fileName) {

        try {
            return filenamesHT.get(fileName);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public String getTestCaseExecutionMode() {

        try {
            return executionMode;
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public String[] getGroups() {

        String[] error;
        error = new String[1];
        error[0] = "error_getGroups()";
        String[] groups;
        int index = 0;
        ArrayList<String> arrGroupList = new ArrayList<String>();
        try {
            Iterator<String> keys = groupsHT.keySet().iterator();
            while (keys.hasNext()) {
                String group = groupsHT.get("Group" + index);
                // log.info(test);
                arrGroupList.add(group);
                keys.next();
                index++;
            }
            groups = new String[arrGroupList.size()];
            arrGroupList.toArray(groups);
            return groups;
        } catch (Exception e) {
            log.error(e.getMessage());
            return error;
        }
    }
}
