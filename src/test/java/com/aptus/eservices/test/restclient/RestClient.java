package com.aptus.eservices.test.restclient;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class RestClient {

    private RestfulClient restClient = null;

    public RestClient() {

        restClient = new RestfulClient();
    }

    public ResponseEntity<String> execute(String url, HttpMethod httpMethod, Object o) {

        ResponseEntity<String> resp = null;
        resp = restClient.execute(url, httpMethod, o);
        return resp;
    }
}
