package com.aptus.eservices.test.restclient;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

import java.net.URI;

public class AptusHttpGetMethod extends HttpEntityEnclosingRequestBase {

    public AptusHttpGetMethod(URI uri) {

        super();
        this.setURI(uri);
    }

    @Override
    public String getMethod() {

        return "GET";
    }
}
