package com.aptus.eservices.test.restclient;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.io.InputStream;

public class AptusClientHttpResponse implements ClientHttpResponse {

    private final HttpResponse httpResponse;

    private HttpHeaders headers;

    public AptusClientHttpResponse(HttpResponse httpResponse) {

        this.httpResponse = httpResponse;
    }

    public InputStream getBody() throws IOException {

        HttpEntity entity = this.httpResponse.getEntity();
        return entity != null ? entity.getContent() : null;
    }

    public HttpHeaders getHeaders() {

        if (this.headers == null) {
            this.headers = new HttpHeaders();
            for (Header header : this.httpResponse.getAllHeaders()) {
                this.headers.add(header.getName(), header.getValue());
            }
        }
        return this.headers;
    }

    public HttpStatus getStatusCode() throws IOException {

        return HttpStatus.valueOf(getRawStatusCode());
    }

    public int getRawStatusCode() throws IOException {

        return this.httpResponse.getStatusLine().getStatusCode();
    }

    public String getStatusText() throws IOException {

        return this.httpResponse.getStatusLine().getReasonPhrase();
    }

    public void close() {

        HttpEntity entity = this.httpResponse.getEntity();
        if (entity != null) {
            try {
                // Release underlying connection back to the connection manager
                EntityUtils.consume(entity);
            } catch (IOException e) {
                // ignore
            }
        }
    }
}
