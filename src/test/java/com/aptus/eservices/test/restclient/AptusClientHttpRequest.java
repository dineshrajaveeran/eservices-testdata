package com.aptus.eservices.test.restclient;

import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.Assert;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

public class AptusClientHttpRequest implements ClientHttpRequest {

    private final HttpClient httpClient;

    private final HttpUriRequest httpRequest;

    private final HttpContext httpContext;

    private final HttpHeaders headers = new HttpHeaders();

    private boolean executed = false;

    private ByteArrayOutputStream bufferedOutput = new ByteArrayOutputStream();

    public AptusClientHttpRequest(HttpClient httpClient, HttpUriRequest httpRequest, HttpContext httpContext) {

        this.httpClient = httpClient;
        this.httpRequest = httpRequest;
        this.httpContext = httpContext;
    }

    public HttpMethod getMethod() {

        return HttpMethod.valueOf(this.httpRequest.getMethod());
    }

    public URI getURI() {

        return this.httpRequest.getURI();
    }

    public final HttpHeaders getHeaders() {

        return (this.executed ? HttpHeaders.readOnlyHttpHeaders(this.headers) : this.headers);
    }

    public final OutputStream getBody() throws IOException {

        checkExecuted();
        return getBodyInternal(this.headers);
    }

    public final ClientHttpResponse execute() throws IOException {

        checkExecuted();
        ClientHttpResponse result = executeInternal(this.headers);
        this.executed = true;
        return result;
    }

    protected ClientHttpResponse executeInternal(HttpHeaders headers) throws IOException {

        byte[] bytes = this.bufferedOutput.toByteArray();
        if (headers.getContentLength() == -1) {
            headers.setContentLength(bytes.length);
        }
        ClientHttpResponse result = executeInternal(headers, bytes);
        this.bufferedOutput = null;
        return result;
    }

    protected ClientHttpResponse executeInternal(HttpHeaders headers, byte[] bufferedOutput) throws IOException {

        for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
            String headerName = entry.getKey();
            if (!headerName.equalsIgnoreCase(HTTP.CONTENT_LEN)
                    && !headerName.equalsIgnoreCase(HTTP.TRANSFER_ENCODING)) {
                for (String headerValue : entry.getValue()) {
                    this.httpRequest.addHeader(headerName, headerValue);
                }
            }
        }
        if (this.httpRequest instanceof HttpEntityEnclosingRequest) {
            HttpEntityEnclosingRequest entityEnclosingRequest = (HttpEntityEnclosingRequest) this.httpRequest;
            ByteArrayEntity requestEntity = (ByteArrayEntity) new ByteArrayEntity(bufferedOutput);
            entityEnclosingRequest.setEntity(requestEntity);
        }
        HttpResponse httpResponse = this.httpClient.execute(this.httpRequest, this.httpContext);
        return new AptusClientHttpResponse(httpResponse);
    }

    private void checkExecuted() {

        Assert.state(!this.executed, "ClientHttpRequest already executed");
    }

    protected OutputStream getBodyInternal(HttpHeaders headers) throws IOException {

        return this.bufferedOutput;
    }
}
