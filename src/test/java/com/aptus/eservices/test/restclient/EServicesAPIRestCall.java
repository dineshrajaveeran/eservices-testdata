package com.aptus.eservices.test.restclient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import java.util.Map;

public interface EServicesAPIRestCall {

    public void setOrgInHeader(String orgid);

    public void setThisHeaderValue(String key, String value);

    public ResponseEntity<String> execute(String url, HttpMethod httpMethod, Object o);

    public ResponseEntity<String> execute(String url, HttpMethod httpMethod, Object o,
                                          Map<String, String> uriVariables);

    public <T> ResponseEntity<T> execute(String url, HttpMethod httpMethod, MultiValueMap<String, String> o,
                                         Class<T> clazz, MultiValueMap<String, String> param);

    public ResponseEntity<String> execute(String url,
                                          HttpMethod httpMethod,
                                          HttpEntity<MultiValueMap<String, String>> req,
                                          MultiValueMap<String, String> param);
}
