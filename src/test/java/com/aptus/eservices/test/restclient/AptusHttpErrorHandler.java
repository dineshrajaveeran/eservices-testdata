package com.aptus.eservices.test.restclient;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.DefaultResponseErrorHandler;

public class AptusHttpErrorHandler extends DefaultResponseErrorHandler {

    public AptusHttpErrorHandler() {

        super();
    }

    @Override
    protected boolean hasError(HttpStatus statusCode) {

        return (false/*
                      * statusCode.series() == HttpStatus.Series.CLIENT_ERROR || statusCode.series() ==
                      * HttpStatus.Series.SERVER_ERROR
                      */);
    }
}
