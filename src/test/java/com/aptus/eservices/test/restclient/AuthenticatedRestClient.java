package com.aptus.eservices.test.restclient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import java.util.Map;

public class AuthenticatedRestClient {

    private RestKeyCloakClient restKeyCloakClient;

    public AuthenticatedRestClient() {

        restKeyCloakClient = new RestKeyCloakClient();
    }

    public AuthenticatedRestClient(String userName, String password) {

        restKeyCloakClient = new RestKeyCloakClient(userName, password);
    }

    public ResponseEntity<String> execute(String url, HttpMethod httpMethod, Object o) {

        ResponseEntity<String> resp = restKeyCloakClient.execute(url, httpMethod, o);
        return resp;
    }

    public void setThisHeaderValue(String key, String value) {

        restKeyCloakClient.setThisHeaderValue(key, value);
    }

    public ResponseEntity<String> execute(String url,
                                          HttpMethod httpMethod,
                                          Object o,
                                          Map<String, String> uriVariables) {

        ResponseEntity<String> resp = restKeyCloakClient.execute(url, httpMethod, o, uriVariables);
        return resp;
    }

    public <T> ResponseEntity<T> execute(String url, HttpMethod httpMethod, MultiValueMap<String, String> o,
                                         Class<T> clazz, MultiValueMap<String, String> param) {

        ResponseEntity<T> resp = restKeyCloakClient.execute(url, httpMethod, o, clazz, param);
        return resp;
    }

    public ResponseEntity<String> excecute(String url,
                                           HttpMethod httpMethod,
                                           HttpEntity<MultiValueMap<String, String>> req,
                                           MultiValueMap<String, String> param) {

        ResponseEntity<String> resp = restKeyCloakClient.execute(url, httpMethod, req, param);
        return resp;
    }
}
