package com.aptus.eservices.test.restclient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestfulClient {

    private static AptusClientHttpRequestFactory factory;

    static {
        factory = new AptusClientHttpRequestFactory();
    }

    private RestTemplate restTemplate;
    private MultiValueMap<String, String> header;
    @SuppressWarnings("rawtypes")
    private HttpEntity request;
    private String endpoint;
    private AptusHttpErrorHandler errorHandler;

    public RestfulClient() {

        header = new LinkedMultiValueMap<String, String>();
        restTemplate = new RestTemplate(factory);
        errorHandler = new AptusHttpErrorHandler();
        restTemplate.setErrorHandler(errorHandler);
        endpoint = "";
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public ResponseEntity<String> execute(String url, HttpMethod httpMethod, Object o) {

        request = new HttpEntity(o, this.header);
        ResponseEntity<String> resp = null;

        try {
            resp = this.restTemplate.exchange(url, httpMethod, request, String.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    public RestTemplate getRestTemplate() {

        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {

        this.restTemplate = restTemplate;
    }

    public MultiValueMap<String, String> getHeader() {

        return header;
    }

    public void setHeader(MultiValueMap<String, String> header) {

        this.header = header;
    }

    public void setThisHeaderValue(String key, String value) {

        this.header.add(key, value);
    }

    @SuppressWarnings("rawtypes")
    public HttpEntity getRequest() {

        return request;
    }

    @SuppressWarnings("rawtypes")
    public void setRequest(HttpEntity request) {

        this.request = request;
    }

    public String getEndpoint() {

        return endpoint;
    }

    public void setEndpoint(String endpoint) {

        this.endpoint = endpoint;
    }

    public RestfulClient setHeader(String propery, String value) {

        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();
        header.set(propery, value);
        RestfulClient client = new RestfulClient();
        client.setHeader(header);
        return client;
    }
}
