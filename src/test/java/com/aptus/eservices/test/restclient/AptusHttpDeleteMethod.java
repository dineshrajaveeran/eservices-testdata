package com.aptus.eservices.test.restclient;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

import java.net.URI;

public class AptusHttpDeleteMethod extends HttpEntityEnclosingRequestBase {

    public AptusHttpDeleteMethod(URI uri) {

        super();
        this.setURI(uri);
    }

    @Override
    public String getMethod() {

        return "DELETE";
    }
}
