package com.aptus.eservices.test.restclient;

import com.aptus.eservices.test.common.LoggerAndReader;
import com.aptus.eservices.test.exception.EServicesException;
import com.aptus.eservices.test.exception.KeycloakHTTPClientSocketException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.OAuth2AccessTokenSupport;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.http.converter.FormOAuth2AccessTokenMessageConverter;
import org.springframework.security.oauth2.http.converter.FormOAuth2ExceptionHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RestKeyCloakClient implements AptusAPIRestCall {

    private static AptusClientHttpRequestFactory factory;

    static {
        factory = new AptusClientHttpRequestFactory();
    }

    private MultiValueMap<String, String> header;
    private AptusHttpErrorHandler errorHandler;
    private OAuth2RestTemplate client;
    private OAuth2AccessToken token;

    protected RestKeyCloakClient() {

        header = new LinkedMultiValueMap<String, String>();
        errorHandler = new AptusHttpErrorHandler();
        DefaultAccessTokenRequest accessTokenRequest = new DefaultAccessTokenRequest();
        DefaultOAuth2ClientContext context = new DefaultOAuth2ClientContext(accessTokenRequest);
        OAuth2AccessTokenSupport support = new OAuth2AccessTokenSupport() {
        };
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new FormOAuth2AccessTokenMessageConverter());
        messageConverters.add(new FormOAuth2ExceptionHttpMessageConverter());
        MappingJackson2HttpMessageConverter jackson = new MappingJackson2HttpMessageConverter();
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(new MediaType("application", "x-www-form-urlencoded"));
        jackson.setSupportedMediaTypes(mediaTypes);
        messageConverters.add(jackson);
        support.setMessageConverters(messageConverters);
        client = new OAuth2RestTemplate(getAuthDetails(null, null), context);
        client.setErrorHandler(errorHandler);
        client.setRequestFactory(factory);
        token = client.getAccessToken();
    }

    protected RestKeyCloakClient(String userName, String userPwd) {

        header = new LinkedMultiValueMap<String, String>();
        errorHandler = new AptusHttpErrorHandler();
        DefaultAccessTokenRequest accessTokenRequest = new DefaultAccessTokenRequest();
        DefaultOAuth2ClientContext context = new DefaultOAuth2ClientContext(accessTokenRequest);
        OAuth2AccessTokenSupport support = new OAuth2AccessTokenSupport() {
        };
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new FormOAuth2AccessTokenMessageConverter());
        messageConverters.add(new FormOAuth2ExceptionHttpMessageConverter());
        MappingJackson2HttpMessageConverter jackson = new MappingJackson2HttpMessageConverter();
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(new MediaType("application", "x-www-form-urlencoded"));
        jackson.setSupportedMediaTypes(mediaTypes);
        messageConverters.add(jackson);
        support.setMessageConverters(messageConverters);
        client = new OAuth2RestTemplate(getAuthDetails(userName, userPwd), context);
        client.setErrorHandler(errorHandler);
        client.setRequestFactory(factory);
        token = client.getAccessToken();
    }

    private ResourceOwnerPasswordResourceDetails getAuthDetails(String userName, String userPwd) {

        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                        return null;
                    }

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {

                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {

                    }
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }

        ResourceOwnerPasswordResourceDetails authDetails = new ResourceOwnerPasswordResourceDetails();
        authDetails.setAccessTokenUri(LoggerAndReader.getInstance().getoAuth2tokenRequestUrl());
        authDetails.setClientId(LoggerAndReader.getInstance().getoAuth2ClientId());
        authDetails.setClientSecret(LoggerAndReader.getInstance().getoAuth2SecretToken());
        authDetails.setGrantType(LoggerAndReader.getInstance().getOauth2granttype());
        if (StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(userPwd)) {
            authDetails.setUsername(userName);
            authDetails.setPassword(userPwd);
        } else {
            authDetails.setUsername(LoggerAndReader.getInstance().getOauth2UserName());
            authDetails.setPassword(LoggerAndReader.getInstance().getOauth2password());
        }
        // authDetails.setScope(Arrays.asList(new String[] {"cn mail sn givenname uid employeeNumber"}));
        return authDetails;
    }

    @Override
    public void setThisHeaderValue(String key, String value) {

        this.header.add(key, value);
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public ResponseEntity<String> execute(String url, HttpMethod httpMethod, Object o) {

        HttpEntity request = new HttpEntity(o, this.header);
        ResponseEntity<String> resp = null;
        this.header.set("Authorization", token.getTokenType() + " " + token.getValue());
        try {
            resp = this.client.exchange(url, httpMethod, request, String.class);
        } catch (Exception e) {
            String str = getStackTrace(e);
            if (StringUtils.containsIgnoreCase(str, "SocketTimeoutException")) {
                throw new KeycloakHTTPClientSocketException(
                        "Got a SocketTimeoutException for URL:" + url + ", HTTPMethod:" + httpMethod);
            } else {
                throw new EServicesException(str);
            }
        }
        return resp;
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public ResponseEntity<String> execute(String url,
                                          HttpMethod httpMethod,
                                          Object o,
                                          Map<String, String> uriVariables) {

        HttpEntity request = new HttpEntity(o, this.header);
        ResponseEntity<String> resp = null;
        try {

            resp = this.client.exchange(url, httpMethod, request, String.class, uriVariables);
        } catch (Exception e) {
            String str = getStackTrace(e);
            if (StringUtils.containsIgnoreCase(str, "SocketTimeoutException")) {
                throw new KeycloakHTTPClientSocketException(
                        "Got a SocketTimeoutException for URL:" + url + ", HTTPMethod:" + httpMethod);
            } else {
                throw new EServicesException(str);
            }
        }
        return resp;
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public <T> ResponseEntity<T> execute(String url, HttpMethod httpMethod, MultiValueMap<String, String> o,
                                         Class<T> clazz, MultiValueMap<String, String> param) {

        HttpEntity request = new HttpEntity(o, this.header);
        ResponseEntity<T> resp = null;
        try {

            resp = this.client.exchange(url, httpMethod, request, clazz, param);
        } catch (Exception e) {

            String str = getStackTrace(e);
            if (StringUtils.containsIgnoreCase(str, "SocketTimeoutException")) {
                throw new KeycloakHTTPClientSocketException(
                        "Got a SocketTimeoutException for URL:" + url + ", HTTPMethod:" + httpMethod);
            } else {
                throw new EServicesException(str);
            }
        }
        return resp;
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public ResponseEntity<String> execute(String url,
                                          HttpMethod httpMethod,
                                          HttpEntity<MultiValueMap<String, String>> req,
                                          MultiValueMap<String, String> param) {

        HttpEntity request = req;
        ResponseEntity<String> resp = null;
        try {
            resp = this.client.exchange(url, httpMethod, request, String.class, param);
        } catch (Exception e) {
            String str = getStackTrace(e);
            if (StringUtils.containsIgnoreCase(str, "SocketTimeoutException")) {
                throw new KeycloakHTTPClientSocketException(
                        "Got a SocketTimeoutException for URL:" + url + ", HTTPMethod:" + httpMethod);
            } else {
                throw new EServicesException(str);
            }
        }
        return resp;
    }

    public String getStackTrace(Throwable t) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }
}
