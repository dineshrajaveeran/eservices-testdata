package com.aptus.eservices.test.restclient;

import org.apache.http.HttpHost;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.util.Assert;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

/**
 * <a href="http://hc.apache.org/httpcomponents-client-ga/httpclient/">Apache HttpComponents HttpClient</a> to create
 * requests.
 * <p>
 * Allows to use a pre-configured {@link HttpClient} instance - potentially with authentication, HTTP connection
 * pooling, etc.
 */

public class AptusClientHttpRequestFactory implements ClientHttpRequestFactory {

    private static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 180;

    private static final int DEFAULT_MAX_CONNECTIONS_PER_ROUTE = 180;

    // changing it 15 minutes. this is a temp setup just to get rid of sockettimeouts...
    private static final int DEFAULT_READ_TIMEOUT_MILLISECONDS = (((1000) * 60) * 15);

    private HttpClient httpClient;

    /**
     * Create a new instance of the HttpComponentsClientHttpRequestFactory with a default {@link HttpClient} that uses a
     * default {@link org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager}.
     */
    public AptusClientHttpRequestFactory() {
        // Client HTTP connection objects when fully initialized can be bound to
        // an arbitrary network socket. The process of network socket initialization,
        // its connection to a remote address and binding to a local one is controlled
        // by a connection socket factory.

        // SSL context for secure connections can be created either based on
        // system or application specific properties.
        SSLContext sslcontext = SSLContexts.createSystemDefault();

        // Create a registry of custom connection socket factories for supported
        // protocol schemes.
        Registry<ConnectionSocketFactory> socketFactoryRegistry =
                RegistryBuilder.<ConnectionSocketFactory>create().register("http",
                                                                           PlainConnectionSocketFactory.INSTANCE)
                        .register("https", new SSLConnectionSocketFactory(sslcontext)).build();

        // Create a connection manager with custom configuration.
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

        // Validate connections after 1 sec of inactivity
        connManager.setValidateAfterInactivity(1000);

        // Configure total max or per route limits for persistent connections
        // that can be kept in the pool or leased by the connection manager.
        connManager.setMaxTotal(DEFAULT_MAX_TOTAL_CONNECTIONS);
        connManager.setDefaultMaxPerRoute(DEFAULT_MAX_CONNECTIONS_PER_ROUTE);
        connManager.setMaxPerRoute(new HttpRoute(new HttpHost("somehost", 80)), 20);

        // Use custom cookie store if necessary.
        CookieStore cookieStore = new BasicCookieStore();

        // Create global request configuration
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.DEFAULT)
                .setExpectContinueEnabled(true)
                .setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
                .setSocketTimeout(DEFAULT_READ_TIMEOUT_MILLISECONDS)
                .setConnectTimeout(DEFAULT_READ_TIMEOUT_MILLISECONDS)
                .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC))
                .build();

        // Create an HttpClient with the given custom dependencies and configuration.
        this.httpClient = HttpClients.custom().setConnectionManager(connManager).setDefaultCookieStore(cookieStore)
                .setDefaultRequestConfig(defaultRequestConfig).build();
    }

    /**
     * Create a new instance of the HttpComponentsClientHttpRequestFactory with the given {@link HttpClient} instance.
     *
     * @param httpClient the HttpClient instance to use for this request factory
     */
    public AptusClientHttpRequestFactory(HttpClient httpClient) {

        Assert.notNull(httpClient, "HttpClient must not be null");
        this.httpClient = httpClient;
    }

    /**
     * Return the {@code HttpClient} used by this factory.
     */
    public HttpClient getHttpClient() {

        return this.httpClient;
    }

    /**
     * Set the {@code HttpClient} used by this factory.
     */
    public void setHttpClient(HttpClient httpClient) {

        this.httpClient = httpClient;
    }

    public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {

        HttpUriRequest httpRequest = createHttpUriRequest(httpMethod, uri);
        postProcessHttpRequest(httpRequest);
        return new AptusClientHttpRequest(getHttpClient(), httpRequest, createHttpContext(httpMethod, uri));
    }

    /**
     * Create a Commons HttpMethodBase object for the given HTTP method and URI specification.
     *
     * @param httpMethod the HTTP method
     * @param uri        the URI
     * @return the Commons HttpMethodBase object
     */
    protected HttpUriRequest createHttpUriRequest(HttpMethod httpMethod, URI uri) {

        switch (httpMethod) {
            case GET:
                return new HttpGet(uri);
            case DELETE:
                return new AptusHttpDeleteMethod(uri);
            case HEAD:
                return new HttpHead(uri);
            case OPTIONS:
                return new HttpOptions(uri);
            case POST:
                return new HttpPost(uri);
            case PUT:
                return new HttpPut(uri);
            case TRACE:
                return new HttpTrace(uri);
            default:
                throw new IllegalArgumentException("Invalid HTTP method: " + httpMethod);
        }
    }

    /**
     * Template method that allows for manipulating the {@link HttpUriRequest} before it is returned as part of a
     * {@link HttpComponentsClientHttpRequest}.
     * <p>
     * The default implementation is empty.
     *
     * @param request the request to process
     */
    protected void postProcessHttpRequest(HttpUriRequest request) {

    }

    /**
     * Template methods that creates a {@link HttpContext} for the given HTTP method and URI.
     * <p>
     * The default implementation returns {@code null}.
     *
     * @param httpMethod the HTTP method
     * @param uri        the URI
     * @return the http context
     */
    protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri) {

        return null;
    }
}
