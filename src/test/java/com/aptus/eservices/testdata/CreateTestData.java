package com.aptus.eservices.testdata;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.aptus.eservices.entity.AccountEntity;
import com.aptus.eservices.entity.CartEntity;
import com.aptus.eservices.entity.DisplayGroupEntity;
import com.aptus.eservices.entity.ManufacturerEntity;
import com.aptus.eservices.entity.OrderEntity;
import com.aptus.eservices.entity.ProductEntity;
import com.aptus.eservices.entity.ProductRequestEntity;
import com.aptus.eservices.entity.ProviderAllocationTierEntity;
import com.aptus.eservices.entity.ProviderEntity;
import com.aptus.eservices.entity.TargetListEntity;
import com.aptus.eservices.entity.UsedAllocationEntity;
import com.aptus.eservices.test.common.DynamoQueryRunner;
import com.aptus.eservices.test.common.IDynamoQueryRunner;
import com.aptus.eservices.test.common.KeyCloakService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CreateTestData {

    private IDynamoQueryRunner queryRunner = new DynamoQueryRunner();
    private KeyCloakService keycloakService = new KeyCloakService();
    private ObjectMapper objectMapper = new ObjectMapper();
    private Logger LOGGER = LogManager.getLogger();

    @Test
    public void createTestData()throws IOException{
        resetAccounts();
        resetCarts();
        resetDisplayGroups();
        resetProducts();
        resetProviders();
        resetProviderAllocationTiers();
        resetManufacturers();
        resetTargetLists();
        resetUsedAllocation();
        resetOrders();
        resetProductRequests();
    }

    private void resetProviderAllocationTiers() throws IOException {

        LOGGER.info("Resetting Provider Allocation Tiers");
        deleteProviderAllocationTiers();
        createProviderAllocationTiers();
    }

    private void resetAccounts() throws IOException {

        LOGGER.info("Resetting Accounts");
        deleteAccounts();
        createAccounts();
    }

    private void resetCarts() {

        LOGGER.info("Resetting Carts");
        deleteCarts();
    }

    private void resetDisplayGroups() throws IOException {

        LOGGER.info("Resetting Display Groups");
        deleteDisplayGroups();
        createDisplayGroups();
    }

    private void resetOrders() throws IOException {

        LOGGER.info("Resetting Orders");
        deleteOrders();
        createOrders();
    }

    private void resetProductRequests() throws IOException {

        LOGGER.info("Resetting Product Requests");
        deleteProductRequests();
        createProductRequests();
    }

    private void resetProducts() throws IOException {

        LOGGER.info("Resetting Products");
        deleteProducts();
        createProducts();
    }

    private void resetProviders() throws IOException {

        LOGGER.info("Resetting Providers");
        deleteProviders();
        createProviders();
    }

    private void resetManufacturers() throws IOException {

        LOGGER.info("Resetting Manufacturers");
        deleteManufacturers();
        createManufacturers();
    }

    private void resetTargetLists() throws IOException {

        LOGGER.info("Resetting Target Lists");
        deleteTargetLists();
        createTargetLists();
    }

    private void resetUsedAllocation() {

        LOGGER.info("Resetting Used Allocation");
        deleteUsedAllocation();
    }

    private void deleteAccounts() {

        List<AccountEntity> accounts = queryRunner.scan(AccountEntity.class, new DynamoDBScanExpression());
        accounts.forEach(account -> queryRunner.delete(account));

        List<UserRepresentation> users = keycloakService.getAllUsers();
        users.stream()
                .filter(p -> !p.getUsername().toLowerCase().equals("identitymanageradmin"))
                .forEach(p -> keycloakService.deleteUser(p.getId()));
    }

    private void deleteCarts() {

        List<CartEntity> carts = queryRunner.scan(CartEntity.class, new DynamoDBScanExpression());
        carts.forEach(cart -> queryRunner.delete(cart));
    }

    private void deleteDisplayGroups() {

        List<DisplayGroupEntity> displayGroups =
                queryRunner.scan(DisplayGroupEntity.class, new DynamoDBScanExpression());
        displayGroups.forEach(displayGroup -> queryRunner.delete(displayGroup));
    }

    private void deleteOrders() {

        List<OrderEntity> orders = queryRunner.scan(OrderEntity.class, new DynamoDBScanExpression());
        orders.forEach(order -> queryRunner.delete(order));
    }

    private void deleteProductRequests() {

        List<ProductRequestEntity> productRequests =
                queryRunner.scan(ProductRequestEntity.class, new DynamoDBScanExpression());
        productRequests.forEach(productRequest -> queryRunner.delete(productRequest));
    }

    private void deleteProducts() {

        List<ProductEntity> products = queryRunner.scan(ProductEntity.class, new DynamoDBScanExpression());
        products.forEach(product -> queryRunner.delete(product));
    }

    private void deleteProviders() {

        List<ProviderEntity> providers = queryRunner.scan(ProviderEntity.class, new DynamoDBScanExpression());
        providers.forEach(provider -> queryRunner.delete(provider));
    }

    private void deleteProviderAllocationTiers() {

        List<ProviderAllocationTierEntity> providerAllocationTiers =
                queryRunner.scan(ProviderAllocationTierEntity.class, new DynamoDBScanExpression());
        providerAllocationTiers.forEach(providerAllocationTier -> queryRunner.delete(providerAllocationTier));
    }

    private void deleteManufacturers() {

        List<ManufacturerEntity> manufacturers =
                queryRunner.scan(ManufacturerEntity.class, new DynamoDBScanExpression());
        manufacturers.forEach(manufacturer -> queryRunner.delete(manufacturer));
    }

    private void deleteTargetLists() {

        List<TargetListEntity> targetLists = queryRunner.scan(TargetListEntity.class, new DynamoDBScanExpression());
        targetLists.forEach(targetList -> queryRunner.delete(targetList));
    }

    private void deleteUsedAllocation() {

        List<UsedAllocationEntity> usedAllocations =
                queryRunner.scan(UsedAllocationEntity.class, new DynamoDBScanExpression());
        usedAllocations.forEach(usedAllocation -> queryRunner.delete(usedAllocation));
    }

    private void createAccounts() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/Account.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray accounts = new JSONArray(json);

        for (int i = 0; i < accounts.length(); i++) {
            createAccount((JSONObject) accounts.get(i));
        }
    }

    private void createAccount(JSONObject account) throws IOException {

        AccountEntity accountEntity = objectMapper.readValue(account.toString(), AccountEntity.class);
        accountEntity.setEmailLowerCase(accountEntity.getEmail().toLowerCase());

        String userId = keycloakService.createNewUser(
                account.getString("firstName"),
                account.getString("lastName"),
                accountEntity.getEmail(),
                account.getString("password"),
                accountEntity.getId());

        accountEntity.setUserId(userId);
        accountEntity.setModelVersion(1);

        queryRunner.save(accountEntity);
    }

    private void createDisplayGroups() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/DisplayGroup.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray products = new JSONArray(json);

        for (int i = 0; i < products.length(); i++) {
            createDisplayGroup((JSONObject) products.get(i));
        }
    }

    private void createDisplayGroup(JSONObject displayGroup) throws IOException {

        DisplayGroupEntity displayGroupEntity =
                objectMapper.readValue(displayGroup.toString(), DisplayGroupEntity.class);
        displayGroupEntity.setModelVersion(1);
        queryRunner.save(displayGroupEntity);
    }

    private void createManufacturers() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/Manufacturer.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray manufacturers = new JSONArray(json);

        for (int i = 0; i < manufacturers.length(); i++) {
            createManufacturer((JSONObject) manufacturers.get(i));
        }
    }

    private void createManufacturer(JSONObject manufacturer) throws IOException {

        ManufacturerEntity manufacturerEntity =
                objectMapper.readValue(manufacturer.toString(), ManufacturerEntity.class);
        manufacturerEntity.setModelVersion(1);
        queryRunner.save(manufacturerEntity);
    }

    private void createOrders() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/Order.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray orders = new JSONArray(json);

        for (int i = 0; i < orders.length(); i++) {
            createOrder((JSONObject) orders.get(i));
        }
    }

    private void createOrder(JSONObject order) throws IOException {

        OrderEntity orderEntity = objectMapper.readValue(order.toString(), OrderEntity.class);
        orderEntity.setModelVersion(1);
        queryRunner.save(orderEntity);
    }

    private void createProviders() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/Provider.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray providers = new JSONArray(json);

        for (int i = 0; i < providers.length(); i++) {
            createProvider((JSONObject) providers.get(i));
        }
    }

    private void createProvider(JSONObject provider) throws IOException {

        ProviderEntity providerEntity = objectMapper.readValue(provider.toString(), ProviderEntity.class);
        providerEntity.setModelVersion(1);
        queryRunner.save(providerEntity);
    }

    private void createProviderAllocationTiers() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/AptusAllocation.ProviderAllocationTier.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray providerAllocationTiers = new JSONArray(json);

        for (int i = 0; i < providerAllocationTiers.length(); i++) {
            createProviderAllocationTier((JSONObject) providerAllocationTiers.get(i));
        }
    }

    private void createProviderAllocationTier(JSONObject providerAllocationTier) throws IOException {

        ProviderAllocationTierEntity providerAllocationTierEntity =
                objectMapper.readValue(providerAllocationTier.toString(), ProviderAllocationTierEntity.class);
        providerAllocationTierEntity.setId(UUID.randomUUID().toString());
        providerAllocationTierEntity.setModelVersion(1);
        queryRunner.save(providerAllocationTierEntity);
    }

    private void createProductRequests() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/ProductRequest.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray productRequests = new JSONArray(json);

        for (int i = 0; i < productRequests.length(); i++) {
            createProductRequest((JSONObject) productRequests.get(i));
        }
    }

    private void createProductRequest(JSONObject productRequest) throws IOException {

        ProductRequestEntity productRequestEntity =
                objectMapper.readValue(productRequest.toString(), ProductRequestEntity.class);
        productRequestEntity.setModelVersion(1);

        //TODO: Read a time interval from the JSONObject to subtract from the current date/time
        productRequestEntity.setRequestDate(new Date());
        queryRunner.save(productRequestEntity);
    }

    private void createProducts() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/Product.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray products = new JSONArray(json);

        for (int i = 0; i < products.length(); i++) {
            createProduct((JSONObject) products.get(i));
        }
    }

    private void createProduct(JSONObject product) throws IOException {

        ProductEntity productEntity = objectMapper.readValue(product.toString(), ProductEntity.class);
        productEntity.setModelVersion(1);
        queryRunner.save(productEntity);
    }

    private void createTargetLists() throws IOException {

        Resource resource = new ClassPathResource("sampledata/default/AptusAllocation.TargetList.json");
        String json = new String(Files.readAllBytes(Paths.get(resource.getURI())));
        JSONArray targetLists = new JSONArray(json);

        for (int i = 0; i < targetLists.length(); i++) {
            createTargetList((JSONObject) targetLists.get(i));
        }
    }

    private void createTargetList(JSONObject targetList) throws IOException {

        TargetListEntity targetListEntity = objectMapper.readValue(targetList.toString(), TargetListEntity.class);
        targetListEntity.setModelVersion(1);
        queryRunner.save(targetListEntity);
    }
}
