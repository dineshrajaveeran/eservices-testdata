package com.aptus.eservices.testdata;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.aptus.eservices.test.common.LoggerAndReader;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CreateDynamoTables {

    static DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(
            new ProfileCredentialsProvider()));

    static SimpleDateFormat dateFormatter = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    static String accountTableName = "Account";
    static String providerTableName = "Provider";
    static String productTableName = "Product";
    static String displayGroupTableName = "DisplayGroup";
    static String manufacturerTableName = "Manufacturer";
    static String providerNpiRegistrationTableName = "ProviderNpiRegistration";
    static String stagedOrderTableName = "StagedOrder";
    static String productRequestTableName = "ProductRequest";
    static String orderSignatureTableName = "OrderSignature";
    static String orderTableName = "Order";
    static String cartTableName = "Cart";
    static String usedAllocationTableName = "AptusAllocation.UsedAllocation";
    static String targetListTableName = "AptusAllocation.TargetList";
    static String allocationTierTableName = "AptusAllocation.ProviderAllocationTier";
    static String dbTablePrefix = LoggerAndReader.getInstance().getDbTablePrefix();

    private void deleteTable(String tableName) {

        Table table = dynamoDB.getTable(tableName);
        try {
            System.out.println("Issuing DeleteTable request for " + tableName);
            table.delete();
            System.out.println("Waiting for " + tableName
                                       + " to be deleted...this may take a while...");
            table.waitForDelete();
        } catch (Exception e) {
            System.err.println("DeleteTable request failed for " + tableName);
            System.err.println(e.getMessage());
        }
    }

    private void createTable(
            String tableName, long readCapacityUnits, long writeCapacityUnits,
            String partitionKeyName, String partitionKeyType) {

        createTable(tableName, readCapacityUnits, writeCapacityUnits,
                    partitionKeyName, partitionKeyType, null, null);
    }

    private void createTable(
            String tableName, long readCapacityUnits, long writeCapacityUnits,
            String partitionKeyName, String partitionKeyType,
            String sortKeyName, String sortKeyType) {

        try {

            ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
            keySchema.add(new KeySchemaElement()
                                  .withAttributeName(partitionKeyName)
                                  .withKeyType(KeyType.HASH)); //Partition key

            ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
            attributeDefinitions.add(new AttributeDefinition()
                                             .withAttributeName(partitionKeyName)
                                             .withAttributeType(partitionKeyType));

            if (sortKeyName != null) {
                keySchema.add(new KeySchemaElement()
                                      .withAttributeName(sortKeyName)
                                      .withKeyType(KeyType.RANGE)); //Sort key
                attributeDefinitions.add(new AttributeDefinition()
                                                 .withAttributeName(sortKeyName)
                                                 .withAttributeType(sortKeyType));
            }

            CreateTableRequest request = new CreateTableRequest()
                    .withTableName(tableName)
                    .withKeySchema(keySchema)
                    .withProvisionedThroughput(new ProvisionedThroughput()
                                                       .withReadCapacityUnits(readCapacityUnits)
                                                       .withWriteCapacityUnits(writeCapacityUnits));

            request.setAttributeDefinitions(attributeDefinitions);

            System.out.println("Issuing CreateTable request for " + tableName);
            Table table = dynamoDB.createTable(request);
            System.out.println("Waiting for " + tableName
                                       + " to be created...this may take a while...");
            table.waitForActive();
        } catch (Exception e) {
            System.err.println("CreateTable request failed for " + tableName);
            System.err.println(e.getMessage());
        }
    }

    private void clearAllTablesAndData() {

        deleteTable(dbTablePrefix + accountTableName);
        deleteTable(dbTablePrefix + providerTableName);
        deleteTable(dbTablePrefix + productTableName);
        deleteTable(dbTablePrefix + displayGroupTableName);
        deleteTable(dbTablePrefix + manufacturerTableName);
        deleteTable(dbTablePrefix + providerNpiRegistrationTableName);
        deleteTable(dbTablePrefix + stagedOrderTableName);
        deleteTable(dbTablePrefix + productRequestTableName);
        deleteTable(dbTablePrefix + orderSignatureTableName);
        deleteTable(dbTablePrefix + orderTableName);
        deleteTable(dbTablePrefix + cartTableName);
        deleteTable(dbTablePrefix + usedAllocationTableName);
        deleteTable(dbTablePrefix + targetListTableName);
        deleteTable(dbTablePrefix + allocationTierTableName);
    }

    private void createAllTables() {

        createTable(dbTablePrefix + providerTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + accountTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + manufacturerTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + displayGroupTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + productTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + providerNpiRegistrationTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + stagedOrderTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + productRequestTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + orderSignatureTableName, 10L, 5L, "orderId", "S");
        createTable(dbTablePrefix + orderTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + cartTableName, 10L, 5L, "id", "S");
        createTable(dbTablePrefix + usedAllocationTableName, 10L, 5L, "providerId", "S");
        createTable(dbTablePrefix + targetListTableName, 10L, 5L, "name", "S");
        createTable(dbTablePrefix + allocationTierTableName, 10L, 5L, "id", "S");
    }

    /**
     * Drop all DynamoDb tables and recreate them
     */
    @Test
    public void deleteAndCreateTables() {

        //clearAllTablesAndData();
        createAllTables();
    }
}
